## Ramas

Las ramas, en un sistema de control de versiones, constituyen una potente herramienta que flexibiliza la forma en la que los colaboradores cooperan en el proyecto (en inglés Branching Workflows). Las ramas son solo una herramienta que es posible utilizar de distintas formas para conseguir distintos objetivos. Veamos algunas posibilidades.

**1.  Ramas de largo recorrido**

En algunos proyectos se tienen varias ramas siempre abiertas, que indican diversos grados de estabilidad del contenido. Por ejemplo, en la rama *‘master’* es frecuente mantener únicamente lo que es totalmente estable. Luego se tienen otras ramas que revelan distintos grados de estabilidad. Por ejemplo podríamos tener una rama 'beta' (versión beta) y otra 'alfa' (versión alfa), en las que se va trabajando. Cuando se alcanza cierto grado de estabilidad superior a la rama en la que se está entonces se realiza una fusión con rama de estabilidad superior.

![Master](https://www.espai.es/blog/wp-content/uploads/2021/05/rama-master-main-git-1.jpg)

**2. Ramas puntuales**

Las ramas puntuales, también llamadas ramas de soporte, son ramas que se crean de forma puntual para realizar una funcionalidad muy concreta. Por ejemplo añadir una nueva característica (se les llama ramas de nueva funcionalidad o directamente por su nombre en inglés *topic branch* o *feature branch*) o corregir un fallo concreto (se les llama ramas para corregir error o directamente por su nombre en inglés *hotfix branch*).

Este tipo de ramas permiten trabajar centrándonos exclusivamente en el desarrollo de una característica concreta y cuando esta esté concluida se fusiona con una de las ramas de largo recorrido (normalmente con la de más bajo nivel de estabilidad, para que sea probada en ese entorno). La fusión solo se realiza cuando se está 'seguro' de que esa característica está correctamente implementada, en lugar de fusionar en el orden que se van desarrollando las cosas. Esto permite por un lado tener un historial de las distintas versiones que se han tenido hasta conseguir la funcionalidad. Por otro lado permiten que el historial de las ramas de largo recorrido no sea 'ensuciados' con distintas modificaciones relativas a una funcionalidad concreta.

El uso de este tipo de ramas permiten más flexibilidad a la hora de probar posibles soluciones. Solo se fusiona con ramas de largo recorrido una vez que estamos seguros de elegir la solución mejor.

Un tipo de ramas de este tipo que tienen un funcionamiento especial son las llamadas ramas de versión o ramas de release (en inglés *release branch*). Este tipo de ramas se crean para dar soporte a la preparación de una nueva versión de producción. Permiten tener bajo control el contenido de la versión y poder realizar cierto mantenimiento sobre ella (añadirle pequeñas mejoras y corrección de errores).

Es frecuente y una buena práctica utilizar en el nombre de la rama un prefijo que indique el tipo de rama de la que se trata. Por ejemplo podría usar *‘feature-’* para ramas de nueva funcionalidad, *‘hotfix-’* para ramas que arreglan errores, y *‘release-’* para ramas de versión. 

![Bifurcado](https://miro.medium.com/v2/resize:fit:4800/format:webp/1*DhagidpZutkaCmAZobmzDQ.png)