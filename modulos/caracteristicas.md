## Características

Git es un sistema de control de versiones distribuido, lo que significa que un clon local del proyecto es un repositorio de control de versiones completo. Estos repositorios locales plenamente funcionales permiten trabajar sin conexión o de forma remota con facilidad. Los desarrolladores confirman su trabajo localmente y, a continuación, sincronizan la copia del repositorio con la del servidor. Este paradigma es distinto del control de versiones centralizado, donde los clientes deben sincronizar el código con un servidor antes de crear nuevas versiones.

**Ventajas**

* Desarrollo simultáneo
* Versiones de lanzamientos mas rápidas
* Integración incorporada
* Sólido soporte técnico de la comunidad
* Funciona con cualquier equipo
* Solicitudes de incorporación de cambios
* Directivas de rama

![ventajas](https://www.ticportal.es/wp-content/uploads/control-versiones-beneficios.jpg)