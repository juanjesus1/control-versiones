# Control-versiones

![Git](img/Git.png)

## ÍNDICE

1. [Introducción](modulos/introduccion.md)
2. [Caracteristicas](modulos/caracteristicas.md)
3. [Terminologia](modulos/terminologia.md)
4. [Flujo de trabajo](modulos/flujo_de_trabajo.md)
5. [Formas de colaborar](modulos/colaborar.md)
6. [Uso de ramas](modulos/ramas.md)

## REFERENCIAS

[Wikipedia](https://es.wikipedia.org/wiki/GitHub)

[Microsoft](https://learn.microsoft.com/es-es/devops/develop/git/what-is-git)

## AUTORES
- Juan Luis
- Jesús
## Licencia
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
